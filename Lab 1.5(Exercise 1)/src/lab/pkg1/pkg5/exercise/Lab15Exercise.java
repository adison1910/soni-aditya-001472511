/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab.pkg1.pkg5.exercise;

import business.Address;
import business.Person;

/**
 *
 * @author USER
 */
public class Lab15Exercise {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Person pn=new Person();
        Address ha=new Address();
        Address la=new Address();
        Address wa=new Address();
        
        pn.setFname("Aditya");
        pn.setLname("Soni");
        pn.setDob("10/19/1994");
        pn.setAddress("120 Park St");
        pn.setSsn("12345");
        
        ha.setAddress1("Rohinipuram DDU Nagar");
        ha.setAddress2("Near Gole Chowk");
        ha.setCity("Raipur");
        ha.setCountry("India");
        ha.setZipcode("492010");
        
        la.setAddress1("120 Park St");
        la.setAddress2("Dorchester");
        la.setCity("Boston");
        la.setCountry("USA");
        la.setZipcode("02122");
        
        wa.setAddress1("360 Huntington Ave");
        wa.setAddress2("Snell Engineering Centre");
        wa.setCity("Boston");
        wa.setCountry("USA");
        wa.setZipcode("02120");
        
        
        System.out.println("Person");
        System.out.println("1. First Name : "+pn.getFname());
        System.out.println("2. Last Name : "+pn.getLname());
        System.out.println("3. Date of Birth :"+pn.getDob());
        System.out.println("4. Address :"+pn.getAddress());
        System.out.println("5. SSN :"+pn.getSsn());
        
        System.out.println("Home Address");
        System.out.println("1. Address1 : "+ha.getAddress1());
        System.out.println("2. Address2 : "+ha.getAddress2());
        System.out.println("3. City :"+ha.getCity());
        System.out.println("4. Country :"+ha.getCountry());
        System.out.println("5. Zipcode :"+ha.getZipcode());
        
        System.out.println("Local Address");
        System.out.println("1. Address1 : "+la.getAddress1());
        System.out.println("2. Address2 : "+la.getAddress2());
        System.out.println("3. City : "+la.getCity());
        System.out.println("4. Country :"+la.getCountry());
        System.out.println("5. Zipcode :"+la.getZipcode());
        
        System.out.println("Work Address");
        System.out.println("1. Address1 : "+wa.getAddress1());
        System.out.println("2. Address2 : "+wa.getAddress2());
        System.out.println("3. City :"+wa.getCity());
        System.out.println("4. Country :"+wa.getCountry());
        System.out.println("5. Zipcode :"+wa.getZipcode());
        
        
    }
    
}
