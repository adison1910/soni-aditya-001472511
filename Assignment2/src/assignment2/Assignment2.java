/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment2;

import Business.Product;
import java.util.Scanner;

/**
 *
 * @author adity
 */
public class Assignment2 {
static Product pro =new Product();
static Scanner cp= new Scanner(System.in);
static boolean check=true;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       
    while(check){
     System.out.println("");
     System.out.println("Enter your operation code like a or b or c.");
     System.out.println( "a.Create Product");
     System.out.println("b.View Product");
     System.out.println("c.Update Product");
     
    String op=cp.nextLine();
    switch(op)
    {
        case "a":
            createRecord();
            break;
                     
        case "b":
            viewRecord();
            break;
                 
        case "c": 
           updateRecord();
          break;
        
    }    
    }
    }
    
    public static void createRecord()
{
    
             System.out.print("Enter your Product Name: ");
             String pname =cp.nextLine();
             pro.setName(pname);
             
              System.out.print("Enter your Product Description: ");
             String pdesc =cp.nextLine();
             pro.setDescription(pdesc);
             
             System.out.print("Enter your Product Price: ");
            double pprice=cp.nextDouble();
            pro.setPrice(pprice);
            
             System.out.print("Enter your Product Availability: ");
             int pavbl=cp.nextInt();
             pro.setAvailNum(pavbl);
             
             System.out.print("Enter your Product's Supplier ID: ");
             int psupid =cp.nextInt();
             pro.getSup().setSupplierID(psupid);
              cp.nextLine();
              
             System.out.print("Enter your Product's Supplier Name: ");
             String psupname =cp.nextLine();
             pro.getSup().setSupplierName(psupname);
             
             System.out.print("Enter your Product's Supplier Address: ");
             String psupadd =cp.nextLine();
             pro.getSup().setSupplierAddr(psupadd);
            
            System.out.println("Record created successfully.");
            System.out.println("");
}

    public static void viewRecord()
    {
             System.out.println("");
             System.out.println("Product Name: "+pro.getName());
             System.out.println("Product Description: "+pro.getDescription());
             System.out.println("Product Price: "+pro.getPrice());
             System.out.println("Product Availability: "+pro.getAvailNum());
             System.out.println("Supplier ID: "+pro.getSup().getSupplierID());
             System.out.println("Supplier Name: "+pro.getSup().getSupplierName());
             System.out.println("Supplier Address: "+pro.getSup().getSupplierAddr());
             System.out.println("");
             System.out.println("");
    }
    
    public static void updateRecord()
    {
            System.out.println("");
            System.out.println("Enter a field code like(i,ii) to be updated");
            System.out.println("i.Product Name | Current value :"+pro.getName());
            System.out.println("ii.Product Description | Current value :"+pro.getDescription());
            System.out.println("iii.Product Price | Current value "+pro.getPrice());
            System.out.println("iv.Product Availability | Current value :"+pro.getAvailNum());
            System.out.println("v.Supplier ID | Current value :"+pro.getSup().getSupplierID());
            System.out.println("vi.Supplier Name | Current value :"+pro.getSup().getSupplierName());
            System.out.println("vii.Supplier Address | Current value :"+pro.getSup().getSupplierAddr());
            System.out.println("Or return back to main menu Please press 'y' ");
            System.out.println("");
           String up=cp.nextLine();
            
            switch(up)
            {
                case "i":
                    System.out.print("Enter your updated Product Name: ");
                    String uppname =cp.nextLine();
                    pro.setName(uppname);
                    System.out.println("Record updated successfully");
                    System.out.println("");
                    updateRecord();
                    break;
                    
                    
                case "ii":
           System.out.print("Enter your updated Product Description: ");
             String uppdesc =cp.nextLine();
             pro.setDescription(uppdesc);
           System.out.println("Record updated successfully");
           System.out.println("");
             updateRecord();
                    break;
                    
                case "iii":
             System.out.print("Enter your updated Product Price: ");
             double uppprice =cp.nextDouble();
             pro.setPrice(uppprice);
             cp.nextLine();
             System.out.println("Record updated successfully");
             System.out.println("");
             updateRecord();
                    break;
                    
                case "iv": 
             System.out.print("Enter your updated Product Availability: ");
             int uppavbl =cp.nextInt();
             pro.setAvailNum(uppavbl);
             cp.nextLine();
             System.out.println("Record updated successfully");
             System.out.println("");
             updateRecord();
                    break;
                    
                case "v":
              System.out.print("Enter your updated Product's Supplier ID: ");
             int uppsupid =cp.nextInt();
             pro.getSup().setSupplierID(uppsupid);
              cp.nextLine();
              System.out.println("Record updated successfully");
              System.out.println("");
              updateRecord();
              break;
            
                case "vi":
              System.out.print("Enter your updated Product's Supplier Name: ");
             String uppsupname =cp.nextLine();
             pro.getSup().setSupplierName(uppsupname);
             System.out.println("Record updated successfully");
             System.out.println("");
             updateRecord();
                    break;
                    
                    
                case "vii":
             System.out.print("Enter your updated Product's Supplier Address: ");
             String uppsupadd =cp.nextLine();
             pro.getSup().setSupplierAddr(uppsupadd);
             System.out.println("Record updated successfully");
             System.out.println("");
             updateRecord();
                    break;
                    
                default:
                break;
                    
            }
    }
    
    
    
    
    
    
    
    
}
