/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;





/**
 *
 * @author USER
 */
public class Product {
    
    
    //Demographic info variables
 private String fname;
 private String lname;
 private String pno ;
 private String dob;
 private String age;
 private String height;
 private String weight;
 private String ssn;
 private String stradd;
 private String city;
 private String state;
 private String zipcode;
 
 //Bank Account Info variables
 private String bankname;
 private String acctype;
 private String routingno;
 private String accno;
 private String balance;
 
 //DL Info variables
 private String dlno;
 private String doi;
 private String doe;
 private String bltype;
 
 //Medical Records Variables
 private String medRecNo;
 private String allg1;
 private String allg2;
 private String allg3;

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getPno() {
        return pno;
    }

    public void setPno(String pno) {
        this.pno = pno;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getSsn() {
        return ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    public String getStradd() {
        return stradd;
    }

    public void setStradd(String stradd) {
        this.stradd = stradd;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public String getAcctype() {
        return acctype;
    }

    public void setAcctype(String acctype) {
        this.acctype = acctype;
    }

    public String getRoutingno() {
        return routingno;
    }

    public void setRoutingno(String routingno) {
        this.routingno = routingno;
    }

    public String getAccno() {
        return accno;
    }

    public void setAccno(String accno) {
        this.accno = accno;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getDlno() {
        return dlno;
    }

    public void setDlno(String dlno) {
        this.dlno = dlno;
    }

    public String getDoi() {
        return doi;
    }

    public void setDoi(String doi) {
        this.doi = doi;
    }

    public String getDoe() {
        return doe;
    }

    public void setDoe(String doe) {
        this.doe = doe;
    }

    public String getBltype() {
        return bltype;
    }

    public void setBltype(String bltype) {
        this.bltype = bltype;
    }

    public String getMedRecNo() {
        return medRecNo;
    }

    public void setMedRecNo(String medRecNo) {
        this.medRecNo = medRecNo;
    }

    public String getAllg1() {
        return allg1;
    }

    public void setAllg1(String allg1) {
        this.allg1 = allg1;
    }

    public String getAllg2() {
        return allg2;
    }

    public void setAllg2(String allg2) {
        this.allg2 = allg2;
    }

    public String getAllg3() {
        return allg3;
    }

    public void setAllg3(String allg3) {
        this.allg3 = allg3;
    }
 
 
    
    

  
 
     
}
