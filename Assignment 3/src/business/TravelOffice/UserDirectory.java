/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.TravelOffice;

import java.util.ArrayList;

/**
 *
 * @author adity
 */
public class UserDirectory {
   public ArrayList<Customer> usrdir;
    public UserDirectory()
    {  
     usrdir=new ArrayList<Customer>();
  }

    public ArrayList<Customer> getUsrdir() {
        return usrdir;
    }
    public Customer addCustomer()
    {
     Customer cus = new Customer();
        usrdir.add(cus);
        return cus;
    }
    
    public void removeCustomer(Customer cus) {
        usrdir.remove(cus);
    }
    
    public Customer searchCustomer(String keyWord) {
        for(Customer cus : usrdir) {
            if(keyWord.equals(cus.getFname())) {
                return cus;
            }
        }
        return null;
    }    

   
    
}


