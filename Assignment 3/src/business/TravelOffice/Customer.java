/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.TravelOffice;

import business.Airliner.Airliner;
import business.TravelAgency.Flight;
import java.util.Date;

/**
 *
 * @author adity
 */
public class Customer {
    private static String fname;
    private static String lname;
    private static Date dob;
    private static int custid;
 
    private Flight ft;
    private Airliner ar;
    public Customer()
    {
            ft=new Flight();
            ar=new Airliner();
    }

    public Flight getFt() {
        return ft;
    }

    public Airliner getAr() {
        return ar;
    }

    public  String getFname() {
        return fname;
    }

    public  void setFname(String fname) {
        Customer.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        Customer.lname = lname;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        Customer.dob = dob;
    }

    public  int getCustid() {
        return custid;
    }

    public void setCustid(int custid) {
        Customer.custid = custid;
    }

    
    
    
}
