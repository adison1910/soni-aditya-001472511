/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.TravelOffice;

import java.util.ArrayList;

/**
 *
 * @author adity
 */
public class CustomerDirectory {
    public ArrayList<Customer> cusdir;
    public CustomerDirectory()
    {
    cusdir=new ArrayList<Customer>();
    }

    public ArrayList<Customer> getCusdir() {
        return cusdir;
    }
    public Customer addCustomer()
    {
     Customer cus = new Customer();
        cusdir.add(cus);
        return cus;
    }
    
    public void removeCustomer(Customer cus) {
        cusdir.remove(cus);
    }
    
    public Customer searchCustomer(String keyWord) {
        for(Customer cus : cusdir) {
            if(keyWord.equals(cus.getFname())) {
                return cus;
            }
        }
        return null;
    }    
    
}
