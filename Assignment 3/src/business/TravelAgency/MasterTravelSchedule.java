/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.TravelAgency;

import business.Airliner.Airliner;
import business.TravelOffice.CustomerDirectory;
import business.TravelOffice.UserDirectory;
import java.util.ArrayList;

/**
 *
 * @author adity
 */
public class MasterTravelSchedule {
   private ArrayList<Flight> masdir;
    private Airliner ar;
    
   
   public MasterTravelSchedule(){
   masdir=new ArrayList<Flight>();    
   
   
   }

   

    public ArrayList<Flight> getMasdir() {
        return masdir;
    }

    public Airliner getAr() {
        return ar;
    }
    public Flight addFlight() {
       Flight fl = new Flight();
        masdir.add(fl);
        return fl;
    }
    
    public void removeFlight(Flight fl) {
       masdir.remove(fl);
    }
    
    public Flight searchFlight(String keyWord) {
        for(Flight fl : masdir) {
            if(keyWord.equals(fl.getFlightId())) {
                return fl;
            }
        }
        return null;
    }

    
   
   
}
