/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.TravelAgency;

import business.TravelOffice.CustomerDirectory;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author adity
 */
public class Flight {
   private String flightId;
   
    private  String source;
    private String destination; 
    private String ptday;
    private static int seatno;
    private  ArrayList[][] seat = new ArrayList[25][6];
    private FlightSchedule fs;
    private CustomerDirectory csdir;
    
    
private Date df;
    public Flight()
    {   
    fs=new FlightSchedule();
    csdir=new CustomerDirectory();
    }

    public CustomerDirectory getCsdir() {
        return csdir;
    }

    public  ArrayList[][] getSeat() {
        return seat;
    }

    

    public FlightSchedule getFs() {
        return fs;
    }

    public Date getDf() {
        return df;
    }

    public void setDf(Date df) {
        this.df = df;
    }

    public String getFlightId() {
        return flightId;
    }

    public void setFlightId(String flightId) {
        this.flightId = flightId;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getPtday() {
        return ptday;
    }

    public void setPtday(String ptday) {
        this.ptday = ptday;
    }

  

    
    public static int getSeatno() {
        return seatno;
    }

    public static void setSeatno(int seatno) {
        Flight.seatno = seatno;
    }

   
    
}
