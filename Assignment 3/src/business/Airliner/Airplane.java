/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.Airliner;

import java.util.Date;

/**
 *
 * @author adity
 */
public class Airplane {
    private static String model;
   private static int serno;
  private static  int pascap;
    private static int fuelcap;
    private static Date nxtmaintdate;
    private static int inc=0;
    public Airplane()
    {
       
    }

    public static String getModel() {
        return model;
    }

    public static void setModel(String model) {
        Airplane.model = model;
    }

    public static int getSerno() {
        return serno;
    }

    public static void setSerno(int serno) {
        Airplane.serno = serno;
    }

    public static int getPascap() {
        return pascap;
    }

    public static void setPascap(int pascap) {
        Airplane.pascap = pascap;
    }

    public static int getFuelcap() {
        return fuelcap;
    }

    public static void setFuelcap(int fuelcap) {
        Airplane.fuelcap = fuelcap;
    }

    public static Date getNxtmaintdate() {
        return nxtmaintdate;
    }

    public static void setNxtmaintdate(Date nxtmaintdate) {
        Airplane.nxtmaintdate = nxtmaintdate;
    }
    
    
    
}
