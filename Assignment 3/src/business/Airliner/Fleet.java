/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.Airliner;

import business.Airliner.Airplane;

/**
 *
 * @author adity
 */
public class Fleet  {
    private Airplane ap;
    private int size;
    
    public Fleet()
    {
        ap=new Airplane();
        size=5;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
    

    public Airplane getAp() {
        return ap;
    }
    
}
