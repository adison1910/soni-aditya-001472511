/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.Airliner;

import business.Airliner.Airliner;

import java.util.ArrayList;

/**
 *
 * @author adity
 */
public class AirlinerDirectory {
    private ArrayList<Airliner> airlinerdir;
   
    public AirlinerDirectory()
    {
    airlinerdir=new ArrayList<Airliner>();
  
    }

    public ArrayList<Airliner> getAirlinerdir() {
        return airlinerdir;
    }

    public void setAirlinerdir(ArrayList<Airliner> airlinerdir) {
        this.airlinerdir = airlinerdir;
    }
    
    public Airliner addAirliner() {
       Airliner air = new Airliner();
        airlinerdir.add(air);
        return air;
    }
    
    public void removeAirliner(Airliner air) {
        airlinerdir.remove(air);
    }
    
    public Airliner searchAirliner(String keyWord) {
        for(Airliner air : airlinerdir) {
            if(keyWord.equals(air.getAlname())) {
                return air;
            }
        }
        return null;
    }
    
    
    
    
    
    
    
}
