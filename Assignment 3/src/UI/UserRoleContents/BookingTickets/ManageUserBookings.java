/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI.UserRoleContents.BookingTickets;

import UI.TravelAgencyContents.BookingTickets.*;
import business.TravelAgency.Flight;
import business.TravelOffice.Customer;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author adity
 */
public class ManageUserBookings extends javax.swing.JPanel {
JPanel rightPanel;
private Flight f; 
    /**
     * Creates new form ViewBookings
     */
    public ManageUserBookings(JPanel rightPanel, Flight f) {
        initComponents();
        this.rightPanel=rightPanel;
        this.f=f;
    }

   

    public void populate(){
        int rowCount = custBookings.getRowCount();
        DefaultTableModel model = (DefaultTableModel)custBookings.getModel();
        
        for(int i = rowCount - 1; i >=0; i--) {
            model.removeRow(i);
        }
        
        for(Customer c : f.getFs().getCsdir())
                {
            Object row[] = new Object[model.getColumnCount()];
            row[0] = c;
            row[1]=c.getFname();
            row[2]=c.getLname();
            row[3]=c.getFt().getFlightId();
            row[4]=c.getFt().getSource();
            row[5]=c.getFt().getDestination();
            model.addRow(row);
            System.out.println(c.getFname());
            System.out.println(c.getLname());
            System.out.println(c.getFt().getFlightId());
            System.out.println(c.getFt().getSource());
            System.out.println(c.getFt().getDestination());
            
        }
        
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        custBookings = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();

        custBookings.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Customer ID", "FirstName", "LastName", "Flight ID", "Source", "Destination"
            }
        ));
        jScrollPane1.setViewportView(custBookings);
        if (custBookings.getColumnModel().getColumnCount() > 0) {
            custBookings.getColumnModel().getColumn(2).setResizable(false);
            custBookings.getColumnModel().getColumn(3).setResizable(false);
        }

        jLabel1.setText("Current Bookings");

        jButton1.setText("View Booking Details");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(53, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(315, 315, 315))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 704, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jButton1)
                                .addGap(301, 301, 301)))
                        .addGap(26, 26, 26))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(28, 28, 28)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addComponent(jButton1)
                .addContainerGap(191, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable custBookings;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
