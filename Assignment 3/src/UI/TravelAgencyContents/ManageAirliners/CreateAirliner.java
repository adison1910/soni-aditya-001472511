/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI.TravelAgencyContents.ManageAirliners;

import business.Airliner.Airliner;
import business.Airliner.AirlinerDirectory;
import java.awt.CardLayout;
import java.awt.Component;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author adity
 */
public class CreateAirliner extends javax.swing.JPanel {
JPanel Cp;
private AirlinerDirectory adir;

    /**
     * Creates new form CreateAirliner
     */
    public CreateAirliner(JPanel Cp, AirlinerDirectory adir) {
        initComponents();
        this.Cp=Cp;
        this.adir=adir;
    }

   

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        airId = new javax.swing.JTextField();
        airName = new javax.swing.JTextField();
        airFSize = new javax.swing.JTextField();
        backBtn = new javax.swing.JButton();
        addAirliner = new javax.swing.JButton();

        jLabel1.setText("Add Airliner");

        jLabel2.setText("ID");

        jLabel3.setText("Name");

        jLabel4.setText("Fleet Size");

        backBtn.setText("<<Back");
        backBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backBtnActionPerformed(evt);
            }
        });

        addAirliner.setText("Add>>");
        addAirliner.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addAirlinerActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(211, 211, 211)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4))
                .addGap(65, 65, 65)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(airId)
                    .addComponent(airName)
                    .addComponent(airFSize, javax.swing.GroupLayout.DEFAULT_SIZE, 147, Short.MAX_VALUE))
                .addContainerGap(144, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(123, 123, 123)
                .addComponent(backBtn)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 195, Short.MAX_VALUE)
                .addComponent(addAirliner)
                .addGap(136, 136, 136))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(255, 255, 255))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(58, 58, 58)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(airId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(33, 33, 33)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(airName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(37, 37, 37)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(airFSize, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 60, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(backBtn)
                    .addComponent(addAirliner))
                .addGap(38, 38, 38))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void backBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backBtnActionPerformed
       Cp.remove(this);    
        Component[] componentArray = Cp.getComponents();
        Component component = componentArray[componentArray.length - 1];
        ManageAirliner mga = (ManageAirliner) component;
        mga.populate();
        CardLayout layout=(CardLayout)Cp.getLayout();
        layout.previous(Cp);
        
        


    }//GEN-LAST:event_backBtnActionPerformed

    private void addAirlinerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addAirlinerActionPerformed
        Airliner air=adir.addAirliner();
        
           if(airId.getText().isEmpty()||airName.getText().isEmpty()||airFSize.getText().isEmpty())
    {
    JOptionPane.showMessageDialog(null, "enter Value(s) in Blank field(s) to be updated.For that first click the update button");
  
    }
    else{
    String id=airId.getText();
    String fsize=airFSize.getText();
    try{
    int aid=Integer.parseInt(id);
    int afsize=Integer.parseInt(fsize);
        air.setId(aid);
        air.getFt().setSize(afsize);
        air.setAlname(airName.getText());
        JOptionPane.showMessageDialog(null, "New Airliner added Successfully");
    }
    
    catch(NumberFormatException e){
    JOptionPane.showMessageDialog(null, "enter Numeric value in Id/Fleet Size field.First press the update button");
    airId.setText("");
    airFSize.setText("");
    }
        
    }//GEN-LAST:event_addAirlinerActionPerformed

    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addAirliner;
    private javax.swing.JTextField airFSize;
    private javax.swing.JTextField airId;
    private javax.swing.JTextField airName;
    private javax.swing.JButton backBtn;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    // End of variables declaration//GEN-END:variables
}
